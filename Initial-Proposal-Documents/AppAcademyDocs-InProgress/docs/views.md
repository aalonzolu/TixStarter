# View Wireframes

See full document:
![design-and-wireframe-pdf]

And, see walk-through.md, in this same folder.

## Home (Not Signed In)
## Home (Signed In)
## Sign In / Sign Up Modal
## EventsIndex with EventsIndexItems
## EventsDetail
## ShowtimeIndex with ShowtimeIndexItems (also available as modal)
## ShowtimeDetail
## TicketIndex with TicketIndexItems (modal)
## Success Modal
## MyTickets
## Edit My Profile Form Modal
## Create/Edit an Event Form Modal
## Create/Edit Showtime Form Modal
## Create/Edit Ticket Form Modal


[design-and-wireframe-pdf]: ./wireframes/TixStarter-Wireframes-and_Design.pdf
