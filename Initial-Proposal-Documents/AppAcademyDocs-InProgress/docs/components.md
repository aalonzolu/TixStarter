## Component Hierarchy

* `App`

  * `EventsIndex`
    * `EventForm`
    * `EventIndexItem`
    * `EventDetail`

  * `ShowtimesIndex`
    * `ShowtimeForm`
    * `ShowtimeIndexItem`
    * `ShowtimeDetail`

  * `TicketsIndex`
    * `TicketForm`
    * `TicketIndexItem`
    * `TicketDetail`
