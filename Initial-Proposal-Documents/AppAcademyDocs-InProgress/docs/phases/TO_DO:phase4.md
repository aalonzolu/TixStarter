
# Phase 4: Allow Complex Styling in Notes (1 day)
This document is still in progress. In the meantime, please refer to the ReadMe or the TixStarter Phases PDF for a description of the various phases. 

## Rails
### Models

### Controllers

### Views

## Flux
### Views (React Components)

### Stores

### Actions

## Gems/Libraries
* react-quill (npm)
